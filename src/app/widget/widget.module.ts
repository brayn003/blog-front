import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';


import { SharedModule } from '../shared/shared.module';
import { WidgetAboutComponent } from './widget.about.component'
// import { ArticleRoutingModule, routedComponents } from './article-routing.module';
// import { ArticleList } from './article.list.component';
// import { ArticleRead } from './article.read.component';
// import { ArticleExcerpt } from './article.excerpt.component';
// import { ArticleDataService } from './article.data.service';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    WidgetAboutComponent
  ],
  exports: [
    WidgetAboutComponent
  ],
  providers:[
  ]
})
export class WidgetModule {}
