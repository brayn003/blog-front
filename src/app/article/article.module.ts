import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { FormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

import { SharedModule } from '../shared/shared.module';
import { WidgetModule } from '../widget/widget.module';
import { ArticleRoutingModule, routedComponents } from './article-routing.module';
import { ArticleList } from './article.list.component';
import { ArticleRead } from './article.read.component';
import { ArticleExcerpt } from './article.excerpt.component';
import { ArticleDataService } from './article.data.service';
import { FoldingCubeComponent } from 'ng-spin-kit/app/spinner/folding-cube'


@NgModule({
  imports: [
    SharedModule,
    WidgetModule,
    ArticleRoutingModule,
    InfiniteScrollModule
  ],
  declarations: [
    routedComponents,
    ArticleExcerpt,
    FoldingCubeComponent
  ],
  providers:[
    ArticleDataService
  ]
})
export class ArticleModule { }
