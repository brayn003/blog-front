import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticleList } from './article.list.component';
import { ArticleRead } from './article.read.component';

const routes: Routes = [{
  path: 'article',
  children: [
    {
      path: '',
      component: ArticleList,
    },
    {
      path: ':slug',
      component: ArticleRead
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ArticleRoutingModule { }

export const routedComponents = [ArticleList, ArticleRead];
