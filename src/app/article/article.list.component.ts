import { Component, OnInit } from '@angular/core';

import { ArticleDataService } from './article.data.service';

@Component({
  selector: 'article-list',
  templateUrl: './view/article.list.html',
  styleUrls: ['./stylesheet/article.list.css']
})
export class ArticleList implements OnInit {

  private articles: Array<any> = [];
  private articlesMeta: any;
  private articleNotLoadingFlag: boolean = false;
  private articleNextUrl: string;
  private disableInfiScroll: boolean = false;

  constructor(
    private articleData: ArticleDataService
  ) { }

  ngOnInit() {
    this.requestArticles();
  }

  private requestArticles() {
    this.articleNotLoadingFlag = false;
    // console.log('requesting')
    this.articleData.getArticles().subscribe((res) => {
      // console.log(res);
      this.articleNotLoadingFlag = true;
      this.setArticles(res.json());
    }, (err) => {
      // console.warn("Error occured while fetching article ids")
    })
  }


  private appendArticles() {
    this.articleNotLoadingFlag = false;
    // console.log('appending articles')
    // console.log(this.articlesMeta['next_page_url']);
    this.articleData.getArticles(this.articlesMeta['next_page_url']).subscribe((res) => {
      this.articleNotLoadingFlag = true;
      this.setArticles(res.json());
      if (this.articlesMeta['next_page_url'] == null) {
        console.log('end of page');
        this.disableInfiScroll = true;
      }
    })
  }

  private setArticles(paginatedArticle) {
    // console.log(paginatedArticle);
    // console.log('thsi')
    Array.prototype.push.apply(this.articles, paginatedArticle['data']);
    // delete paginatedArticle['data'];
    // console.log(this.articles);
    this.articlesMeta = paginatedArticle;
  }

  private onScroll() {
    // console.log("Hukahuahaha", this.articleNotLoadingFlag);
    if (this.articleNotLoadingFlag) {
      this.appendArticles();
    }
  }

  private onScrollUp() {
  }

}
