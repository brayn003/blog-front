import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class ArticleDataService {

  constructor(private http: Http) { }

  getArticles(url?:string){
    // let tempUrl = url != undefined ? url : 'http://localhost:8000/api/article';
    // console.log('requesting to',tempUrl);
    // return this.http.get(tempUrl);
    return this.http.get(url != undefined ? url : environment.apiUrl+'api/article');
  }

  getArticleSlugs(){
    return this.http.get(environment.apiUrl+'api/article-slugs');
  }

  getArticleIds(){
    console.log('inhere')
    return this.http.get(environment.apiUrl+'api/article-ids');
  }

  getArticleBySlug(slug : string){
    return this.http.get(environment.apiUrl+'api/article-slugs/'+slug);
  }

  getArticleById(id : number){
    return this.http.get(environment.apiUrl+'api/article/'+id);
  }

}