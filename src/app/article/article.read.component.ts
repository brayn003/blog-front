import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { ArticleDataService } from './article.data.service';

@Component({
  selector: 'article-read',
  templateUrl: './view/article.read.html',
  styleUrls: ['./stylesheet/article.read.css']
})
export class ArticleRead implements OnInit {

  private article: any = {};

  constructor(
    private route: ActivatedRoute,
    private articleData: ArticleDataService
  ) { }

  ngOnInit() {
    this.route.params
      // (+) converts string 'id' to a number
      .switchMap((params: Params) => this.articleData.getArticleBySlug(params['slug']))
      .subscribe((article) => {
        this.article = article.json()
        console.log(this.article);
      });

  }

  

}
