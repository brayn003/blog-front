import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ArticleDataService } from './article.data.service';

@Component({
  selector: 'article-excerpt',
  templateUrl: './view/article.excerpt.html',
  styleUrls: ['./stylesheet/article.excerpt.css']
})
export class ArticleExcerpt implements OnInit {
  @Input() excerpt: any = {};

  constructor(
    private router: Router,
    private articleData: ArticleDataService
  ) { }

  ngOnInit() {
    // console.log(this.excerpt);
  }

  private onClickContinueReading(slug: string) {
    this.router.navigate(['/article', slug]);
  }

}
