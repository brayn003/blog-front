import { Component } from '@angular/core';

@Component({
  selector: 'the-blog-site',
  templateUrl: './view/core.root.html'
})
export class CoreRootComponent {}
