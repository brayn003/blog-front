import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreRootComponent } from './core.root.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'article', pathMatch: 'full' }
    ]),
  ],
  exports: [
    CoreRootComponent
  ],
  declarations: [
    CoreRootComponent
  ],
  providers: [],
})
export class CoreModule { }
