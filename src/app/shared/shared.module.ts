import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
// import { FormsModule } from '@angular/forms';
import { CarouselModule } from 'ng2-bootstrap/carousel';
import { SharedCarousel } from './shared.carousel.component';
import { NavbarMainComponent } from './navbar.main.component'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    CarouselModule.forRoot()
  ],
  exports:[
    CommonModule,
    FormsModule,
    HttpModule,
    SharedCarousel,
    NavbarMainComponent
  ],
  declarations: [
    SharedCarousel,
    NavbarMainComponent
  ],
  providers:[
  ]
})
export class SharedModule { }
