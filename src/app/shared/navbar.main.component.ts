import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'navbar-main',
  templateUrl: './view/navbar.main.html',
  styleUrls: ['./stylesheet/navbar.main.css']
})
export class NavbarMainComponent {

  private menu : Array<any>;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ){
    this.menu = this.menuItems();
    this.activeRoute();
  }

  private menuItems(){
    return [{
      name : 'Home',
      route : ''
    },{
      name : 'Article',
      route : 'article'
    }]
  }

  private onClickMenuItem(route:string){
    console.log('clicked route',route)
    this.router.navigate([route]);    
  }

  private activeRoute(){
    // this.activeRoute.
    console.log(this.route.url);
  }
}
